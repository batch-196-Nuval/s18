//alert("Hello World");

function printSum(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
};
printSum(5, 15);

function printDifference(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
};
printDifference(20, 5);

function returnProduct(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
}
let product = returnProduct(50, 10);
console.log(product);

function returnQuotient(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}
let quotient = returnQuotient(50, 10);
console.log(quotient);

function returnCircleArea(num1){
	let pi = 3.1416;
	console.log("The result of getting the area of a circle with " + num1 + " radius:");
	return pi * (num1 ** 2);
}
let circleArea = returnCircleArea(15);
console.log(circleArea);

function returnAverage(num1, num2, num3, num4){
	/*let sum = num1 + num2 + num3 +num4;*/
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":");
	/*return sum / 4;*/
	return (num1 + num2 + num3 + num4) / 4;
}
let averageVar = returnAverage(20, 40, 60, 80);
console.log(averageVar);

function returnPercentage(num1, num2){
	let percentage = (num1 / num2) * 100;
	let isPassed = percentage >= 75;
	console.log("Is " + num1 + "/" + num2 + " a passing score?");
	return isPassed;
}
let isPassingScore = returnPercentage(38, 50);
console.log(isPassingScore);

