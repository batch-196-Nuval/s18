// Function able to receive data without the use of global variable or prompt();

// "name" - is a parameter
// A parameter is a varaible / container that exists only in our function and is used to store information that is provided to a function when it is called / invoked
function printName(name){
	console.log("May name is " + name);
};
// Data passed into a function invocation can be received by the function
// This is what we can an argument
printName("Juana");
printName("Squall");

// Data passed into the function through function invocation is called an argument

// The argument is then stored within a container called a parameter.

function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

// 1.
	function DisplaySuperhero(hero){
		console.log("My favorite Superhero is " + hero);
	}
	DisplaySuperhero("Spiderman");


// 2.
	function printMyFavoriteLanguage(language){
		console.log("My favorite language is: " + language);
	};

	printMyFavoriteLanguage("Javascript");
	printMyFavoriteLanguage("Java");

// Multiple Arguments can also be passed into a function; Multiple parameters can contain our arguments

function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan", "Crisostomo", "Ibarra");
/*
	Parameters will contain the arguments according to the order it was passed
	"Juan" - firstName
	"Crisostomo" - middleName
	"Ibarra" - lastName

	In other languages, providing more / less arguments than expected parameters sometimes cause an error or changes the behavior of the function.

	In Javascript, we don't have to worry about that.

	In Javascript, providing less arguments than the expected parameters wil automatically assign an undefined value to the parameter
*/
// commas are used to separate values
printFullName("Stephen", "Wardell");
printFullName("Stephen", "Wardell", "Curry");
printFullName("Stephen", "Wardell", "Curry", "James");

// Use Variables as Arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);

function DisplayFiveFavSong(NumberOneSong, NumberTwoSong, NumberThreeSong, NumberFourSong, NumberFiveSong){
	console.log(NumberOneSong + ", " + NumberTwoSong + ", " + NumberThreeSong + ", " + NumberFourSong + ", and " + NumberFiveSong);
};
DisplayFiveFavSong("Song1", "Song2", "Song3", "Song4", "Song5");

// Return Statement

// Currently or so far, our functions are able tom display data in our console.
// However, our functions cannot yet return values. Functions are able to to return values which can be saqved into a variable using the return statement / keyword

let fullName = printFullName("Mark", "Joseph", "Lacdao");
console.log(fullName); // undefined

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
};

fullName = returnFullName("Ernesto", "Antonio", "Maceda");
console.log(fullName);

// Functions which have a return statement are able to rturn value and it can be saved in a variable.

console.log(fullName + " is my grandpa.");


function returnPhilippineAddress(city){

	return city + ", " + "Philippines";

};

// return a value from a function which we can save in a variable
let myFullAddress = returnPhilippineAddress("Cainta");
console.log(myFullAddress);

function checkDivisibilityBy4(num){
	let remainder = num % 4;
	let isDivisibleBy4 = remainder === 0;

	// returns value either true or false
	// Not only can you return raw value / data, you can also directly return a variable
	return isDivisibleBy4;
	// return keyword not only allows us to return value but also ends the process of our function
	console.log("I am ran after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);


// 1.
function createPlayerInfo(username, level, job){

	return "username: " + username + ", level: " + level + ", job: " + job;

};

let user1 = createPlayerInfo("white_night", 95, "Paladin");
console.log(user1);

